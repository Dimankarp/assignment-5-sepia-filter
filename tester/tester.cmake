# CMP0007: list command no longer ignores empty elements.
if(POLICY CMP0007)
    cmake_policy(SET CMP0007 NEW)
endif()
set(ELAPSED)
function(exec_check)
    execute_process(COMMAND ${ARGV}
        OUTPUT_VARIABLE out
        ERROR_VARIABLE  err
        RESULT_VARIABLE result)

    string(REGEX MATCH "\\[TRNSF_ELAPSED\\]: ([0-9]+)" elapsed_time ${err} )
    set(ELAPSED ${ELAPSED} ${CMAKE_MATCH_1} PARENT_SCOPE)


    if(result)
        string(REPLACE "/" ";" name_components ${ARGV0})
        list(GET name_components -1 name)
        if(NOT out)
            set(out "<empty>")
        endif()
        if(NOT err)
            set(err "<empty>")
        endif()
        message(FATAL_ERROR "\nError running \"${name}\"\n*** Output: ***\n${out}\n*** Error: ***\n${err}\n")
    endif()
endfunction()

file(REMOVE ${TEST_DIR}/output_slow.bmp)
file(REMOVE ${TEST_DIR}/output_fast.bmp)
exec_check(${IMAGE_TRANSFORMER} -f ${TEST_DIR}/input.bmp ${TEST_DIR}/output_slow.bmp)
exec_check(${IMAGE_TRANSFORMER} -fF ${TEST_DIR}/input.bmp ${TEST_DIR}/output_fast.bmp)


list(GET ELAPSED 0 slow_time)
list(GET ELAPSED 1  fast_time)
math(EXPR percent_diff "${slow_time} * 100 / ${fast_time} - 100")
message("\n Slow algorithm took: ${slow_time} units.\n Fast algorithm took: ${fast_time} units.\n The SSE-ful algorithm is by ${percent_diff}% faster.\n")

if(NOT fast_time LESS slow_time)
    message(FATAL_ERROR "\nError running slow and fast implementation: The fast implementation is slower! FAST:\"${fast_time}\"\n SLOW:\"${slow_time}\"\n")
endif()
